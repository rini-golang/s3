package s3

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"strings"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/rs/zerolog/log"
	"gitlab.com/rini-golang/config"
)

type S3 struct {
	// Config *aws.Config
	MinioClient *minio.Client
}

func NewStorage(cfg *config.S3) (*S3, error) {
	// Сделать автоматическое определение разных S3 провайдеров: яндекс, минио, aws ...
	if cfg == nil {
		log.Debug().Err(errors.New("S3 config is empty")).Msg("")
		return nil, errors.New("S3 config is empty")
	}

	minioClient, err := minio.New(cfg.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(cfg.AccessKey, cfg.SecretKey, ""),
		Secure: cfg.Secure,
	})
	if err != nil {
		log.Debug().Err(err).Msg("")
		return nil, err
	}

	s := &S3{}
	s.MinioClient = minioClient

	return s, nil
}

// https://github.com/awsdocs/aws-doc-sdk-examples/tree/main/go/example_code/s3
// func (s *S3) PutObject(ctx context.Context, fullPath string, file []byte) error {

// 	bucket := strings.Split(fullPath, "/")[0]
// 	filePath := aws.String(strings.Replace(fullPath, fmt.Sprintf("%s/", bucket), "", -1))

// 	newSession := session.New(s.Config)
// 	svc := s3.New(newSession)

// 	_, err := svc.PutObject(&s3.PutObjectInput{
// 		Body:   bytes.NewReader(file),
// 		Bucket: &bucket,
// 		Key:    filePath,
// 	})
// 	if err != nil {
// 		log.Debug().Msgf("Failed to upload data to %s/%s, %s\n", bucket, *filePath, err)
// 		return err
// 	}
// 	return nil
// }

// func (s *S3) GetObject(ctx context.Context, fullPath string) (io.ReadCloser, *int64, error) {
// 	bucket := strings.Split(fullPath, "/")[0]
// 	filePath := aws.String(strings.Replace(fullPath, fmt.Sprintf("%s/", bucket), "", -1))

// 	newSession := session.New(s.Config)
// 	svc := s3.New(newSession)

// 	headObj := s3.HeadObjectInput{
// 		Bucket: aws.String(bucket),
// 		Key:    filePath,
// 	}
// 	result, err := svc.HeadObject(&headObj)
// 	if err != nil {
// 		log.Debug().Err(err).Msg("")
// 		return nil, nil, err
// 	}
// 	obj, err := svc.GetObject(&s3.GetObjectInput{
// 		Bucket: &bucket,
// 		Key:    filePath,
// 	})
// 	if err != nil {
// 		log.Debug().Msgf("Failed to download data to %s/%s, %s\n", bucket, *filePath, err)
// 		return nil, nil, err
// 	}
// 	return obj.Body, result.ContentLength, nil
// }

// func (s *S3) CreateStorage(ctx context.Context, bucket string) error {

// 	newSession := session.New(s.Config)
// 	svc := s3.New(newSession)

// 	_, err := svc.CreateBucket(&s3.CreateBucketInput{
// 		Bucket: &bucket,
// 	})
// 	if err != nil {
// 		log.Debug().Err(err).Msgf("Failed to create bucket: %s", err)
// 		return err
// 	}

// 	if err = svc.WaitUntilBucketExists(&s3.HeadBucketInput{Bucket: &bucket}); err != nil {
// 		log.Debug().Msgf("Failed to wait for bucket to exist %s, %s\n", bucket, err)
// 		return err
// 	}
// 	return nil
// }

func (s *S3) PutObject(ctx context.Context, bucket, path, fileName string, file []byte) error {
	readerForMinio := bytes.NewReader(file)

	_, err := s.MinioClient.PutObject(ctx, bucket, fmt.Sprintf("%s/%s", path, fileName), readerForMinio, int64(len(file)), minio.PutObjectOptions{})
	if err != nil {
		log.Debug().Err(err).Msg("")
		return err
	}

	return nil
}

func (s *S3) GetObject(ctx context.Context, bucket, filePath, fileName string) (io.Reader, error) {
	// bucket := strings.Split(path, "/")[0]
	// filePath := strings.Replace(path, fmt.Sprintf("%s/", bucket), "", 1)
	// key := aws.String(strings.Replace(path, fmt.Sprintf("%s/", bucket), "", -1))
	// log.Debug().Msgf("%s", bucket)
	// for obj := range s.MinioClient.ListObjects(ctx, "maps", minio.ListObjectsOptions{}) {
	// 	log.Debug().Msgf("%+v", obj)
	// }
	// log.Debug().Msgf("%s", bucket)
	// log.Debug().Msgf("%s", filePath)
	object, err := s.MinioClient.GetObject(ctx, bucket, fmt.Sprintf("%s/%s", filePath, fileName), minio.GetObjectOptions{})
	if err != nil {
		log.Debug().Err(err).Msg("")
		return nil, err
	}

	stat, err := object.Stat()
	if err != nil {
		log.Debug().Err(err).Msg("")
		return nil, err
	}

	log.Debug().Msgf("%+v", stat)

	return object, nil
}

func (s *S3) GetsObject(ctx context.Context, path string) (io.Reader, error) {
	bucket := strings.Split(path, "/")[0]
	filePath := strings.Replace(path, fmt.Sprintf("%s/", bucket), "", 1)

	// key := aws.String(strings.Replace(path, fmt.Sprintf("%s/", bucket), "", -1))
	// log.Debug().Msgf("%s", bucket)

	// for obj := range s.MinioClient.ListObjects(ctx, "maps", minio.ListObjectsOptions{}) {
	// 	log.Debug().Msgf("%+v", obj)
	// }

	object, err := s.MinioClient.GetObject(ctx, bucket, filePath, minio.GetObjectOptions{})
	if err != nil {
		log.Debug().Err(err).Msg("")
		return nil, err
	}

	downloadInfoBytes, err := io.ReadAll(object)
	if err != nil {
		log.Debug().Err(err).Msg("")
		return nil, err
	}

	log.Info().Msg(string(downloadInfoBytes))

	return object, nil
}
